
var children = document.querySelectorAll(".header__menu-item");
children.forEach(function (child) {
  var link = child.querySelector(".header__menu-link");
  var sub = child.querySelector(".submenu");

  link.addEventListener("click", function () {
    if (sub.style.maxHeight) {
      sub.style.maxHeight = null;
      child.classList.remove("open");
    } else {
      sub.style.maxHeight = sub.scrollHeight + 40 + "px";
      child.classList.add("open");
    }
  });
});

var menuMobi = document.querySelectorAll(".container-fuild");
menuMobi.forEach(function (child) {
  let link = child.querySelector(".icon-menu");
  let sub = child.querySelector(".header__menu");
  let subBtn = child.querySelector(".sub-action-mobie");
  let subItem = child.querySelector(".header__menu-items");

  link.addEventListener("click", function () {
    if (sub.style.maxHeight) {
      sub.style.maxHeight = null;
      child.classList.remove("show-menu-mobi");
    } else {
      child.classList.add("show-menu-mobi");
      sub.style.maxHeight = sub.scrollHeight + "px";
    }
  });
});

$(".btn-close").on("click", function () {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(".container-fuild").removeClass("show-menu-mobi");
    $(".header__menu").css("max-height", "");
  } else {
    $(".submenu").css("max-height", "");
    $(".header__menu-item").removeClass("open");
  }
});

// func back to top
function bttBtn() {
  $("html, body").animate({ scrollTop: 0 }, 1000);
}
